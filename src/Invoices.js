import {
  TOKEN_TIME_LIMIT,
  URI_ROOT_LIVE,
  URI_ROOT_SANDBOX,
  OAUTH_PATH,
} from "./constants"
import "./util"
import {
  OAuthToken,
  Invoice,
  Invoicer,
  Link,
  InvoiceList,
  AmountRange,
  DateTimeRange,
  ShippingInfo
} from "./models"
import { Page } from "./pagination"
import { default as axiosDefault } from "axios"

if (process.env.PHASE === "DEVELOPMENT") {
  axiosDefault.interceptors.request.use((request) => {
    console.log("Starting Request", request)
    return request
  })

  axiosDefault.interceptors.response.use((response) => {
    console.log("Response:", response)
    return response
  })
}

/**
 * Invoice API Instance.
 */
export default class Invoices {
  /**
   * @param {string} clientId
   * @param {string} clientSecret
   * @param {boolean} sandbox - Whether this instance will use the sandbox API
   */
  constructor(clientId, clientSecret, sandbox) {
    /** Public client ID */
    this.clientId = clientId

    /** Secret client key */
    this.clientSecret = clientSecret
    /** Whether this instnace is using Paypal snadbox or not */
    this.sandbox = sandbox === true
    this.baseUri = sandbox ? URI_ROOT_SANDBOX : URI_ROOT_LIVE
    /** Whether this instance has complete initializtion */
    this.initialized = false
    /**
     * OAuth token
     *
     * @type {OAuthToken}
     */
    this.oathToken
  }

  /**
   * Initializes Auth and axios instances.
   *
   * @returns {Promise<Invoices>} This instance, now initialized.
   */
  async initialize() {
    // Get token
    this.oathToken = await this.refreshToken()

    // Initialize axios instance
    await this.axios()

    this.initialized = true

    return this
  }

  /**
   * Gets a new OAuth Token.
   *
   * @returns {Promise<OAuthToken>}
   */
  async refreshToken() {
    const result = await axiosDefault.post(
      this.baseUri + OAUTH_PATH,
      "grant_type=client_credentials",
      {
        auth: {
          username: this.clientId,
          password: this.clientSecret,
        },
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      }
    )

    return new OAuthToken(result.data)
  }

  /**
   * Gets the current axios instance or updates it with a refreshed token.
   */
  async axios() {
    const tokenExpired =
      !this.oathToken || this.oathToken.minutesLeft() < TOKEN_TIME_LIMIT

    // If token expired, refresh the token
    if (tokenExpired) {
      this.oathToken = await this.refreshToken()
    }

    // If axios wasn't created or the token expired, generate axios
    if (!this._axios || tokenExpired) {
      /** Axios instance with valid token */
      this._axios = axiosDefault.create({
        url: this.baseUri,
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${this.oathToken.accessToken}`,
        },
      })
    }

    return this._axios
  }

  //--------------
  // Setting defaults

  /**
   * @param {Invoicer} [newDefault] - The invoicer passed to update the default invoice.
   * If no value is passed the function returns the current default.
   *
   * @returns {Invoicer|null} The default invoicer, if a new default was passed it returns the new one.
   */
  defaultInvoicer(newDefault) {
    if (newDefault) {
      this._defaultInvoicer = newDefault
    }

    return this._defaultInvoicer
  }

  //--------------
  // API FUNCTIONS

  /**
   * Generates the next invoice number that is available to the merchant.
   * The next invoice number uses the prefix and suffix from the last
   * invoice number and increments the number by one.
   * For example, the next invoice number after INVOICE-1234 is INVOICE-1235.
   *
   * @returns {Promise<string>} next invoice number
   */
  async generateInvoiceNumber() {
    const axios = await this.axios()

    const result = await axios.post(
      `${this.baseUri}/v2/invoicing/generate-next-invoice-number`
    )

    return result.data.invoice_number
  }

  /**
   * Creates a draft invoice. To move the invoice from a draft to
   * payable state, you must send the invoice.
   *
   * The invoice object must include an items array.
   *
   * Note: The merchant that you specify in an invoice must have a PayPal account in good standing.
   *
   * @param {Invoice} invoice - The invoice draft to create.
   *
   * Notes:
   * - If a default invoicer is set, it can be omitted from the passed object.
   * @returns {Promise<Link>} A Link which gives the API endpoint to GET the created invoice.
   *
   * @see The Docs say that this endpoint returns the created Invoice. It does not...
   * @see [Docs](https://developer.paypal.com/docs/api/invoicing/v2/#invoices_create)
   */
  async createDraftInvoice(invoice) {
    const axios = await this.axios()

    if (!invoice.invoicer && !this.defaultInvoicer()) {
      throw new Error(
        "Missing invoicer and no default invoicer set." +
        " Use `api.defaultInvoicer(newDefault)` or add an invoicer to the invoice."
      )
    }

    const result = await axios.post(
      `${this.baseUri}/v2/invoicing/invoices`,
      invoice.invoicer
        ? invoice
        : { ...invoice, invoicer: this.defaultInvoicer() }
    )

    return result.data
  }

  /**
   * Get details for an invoice, by ID.
   *
   * @param {string} invoiceID
   * @returns {Promise<Invoice>}
   *
   * @see [Docs](https://developer.paypal.com/docs/api/invoicing/v2/#invoices_get)
   */
  async getInvoiceByID(invoiceID) {
    const axios = await this.axios()

    const result = await axios.get(
      `${this.baseUri}/v2/invoicing/invoices/${invoiceID}`
    )

    return result.data
  }

  /**
   * Get details for an invoice, by an API URI.
   *
   * @param {string} invoiceID
   * @returns {Promise<Invoice>}
   *
   * @see [Docs](https://developer.paypal.com/docs/api/invoicing/v2/#invoices_get)
   */
  async getInvoiceByUri(uri) {
    const axios = await this.axios()

    const result = await axios.get(uri)

    return result.data
  }

  /**
   * Get details for an invoice using a Link object from the API.
   *
   * @param {Link} link
   * @returns {Promise<Invoice>}
   *
   * @see [Docs](https://developer.paypal.com/docs/api/invoicing/v2/#invoices_get)
   */
  async getInvoiceByLink(link) {
    const axios = await this.axios()

    const result = await axios.get(link.href)

    return result.data
  }

  /**
   * Searches for and lists invoices that match search criteria.
   * If you pass multiple criteria, the response lists invoices that match all criteria.
   * 
   * @param {Object} [s]
   * @param {number} [s.page] - The page number to be retrieved
   * @param {number} [s.pageSize] - The page size for the search results.
   * @param {string} [s.recipientEmail] - Filters the search by email address
   * @param {string} [s.recipientFirstName] - Filters the search by the recipient first name
   * @param {string} [s.recipientLastName] - Filters the search by the recipient last name
   * @param {string} [s.recipientBusinessName] - Filters the search by the recipient business name
   * @param {number} [s.invoiceNumber] - Filters the search by the invoice number
   * @param {Array<
   *          "PAYMENT_PENDING"|"UNPAID"|"MARKED_AS_REFUNDED"|"PARTIALLY_REFUNDED"|"PARTIALLY_PAID"|"REFUNDED"|
   *          "CANCELLED"|"MARKED_AS_PAID"|"PAID"|"SCHEDULED"|"SENT"|"DRAFT"
   *        >|"PAYMENT_PENDING"|"UNPAID"|"MARKED_AS_REFUNDED"|"PARTIALLY_REFUNDED"|"PARTIALLY_PAID"|"REFUNDED"|
   *          "CANCELLED"|"MARKED_AS_PAID"|"PAID"|"SCHEDULED"|"SENT"|"DRAFT"
   *               } [s.status] - An array of InvoiceStatus strings
   * @param {string} [s.reference] - The reference data, such as a PO number
   * @param {string} [s.currencyCode] - The three-character ISO-4217 currency code that identifies the currency.
   * @param {string} [s.memo] - A private bookkeeping memo for the user.
   * @param {AmountRange} [s.totalAmountRange] - Filters the search by the total amount
   * @param {DateTimeRange} [s.invoiceDateRange] - Filters the search by a date range for the invoice
   * @param {DateTimeRange} [s.dueDateRange] - Filters the search by a due date range for the invoice
   * @param {DateTimeRange} [s.paymentDateRange] - The date and time range. Filters invoices by creation date, invoice date, due date, and payment date.
   * @param {DateTimeRange} [s.creationDateRange] - Filters the search by a creation date range for the invoice
   * @param {boolean} [s.archived] - Indicates whether to list merchant-archived invoices in the response. Value is:
                                   true: Response lists only merchant-archived invoices.
                                   false: Response lists only unarchived invoices.
                                   null: Response lists all invoices.

   * @returns {Promise<Page>} A page of results
   * 
   * @see [Docs](https://developer.paypal.com/docs/api/invoicing/v2/#search-invoices)
   */
  async search({
    page,
    pageSize,
    recipientEmail,
    recipientFirstName,
    recipientLastName,
    recipientBusinessName,
    invoiceNumber,
    status,
    reference,
    currencyCode,
    memo,
    totalAmountRange,
    invoiceDateRange,
    dueDateRange,
    paymentDateRange,
    creationDateRange,
    archived,
  } = {}) {
    const axios = await this.axios()

    const result = await axios.post(
      `${this.baseUri}/v2/invoicing/search-invoices`,
      {
        recipientEmail,
        recipientFirstName,
        recipientLastName,
        recipientBusinessName,
        invoiceNumber,
        status: Array.isArray(status) ? status : [status],
        reference,
        currencyCode,
        memo,
        totalAmountRange,
        invoiceDateRange,
        dueDateRange,
        paymentDateRange,
        creationDateRange,
        archived,
      },
      {
        params: { page, page_size: pageSize, total_required: true }
      }
    )

    return result.data
  }

  /**
   * Lists invoices.
   *
   * @param {number} [page=1] - The page number to be retrieved, for the list of items.
   *                        So, a combination of page=1 and page_size=20 returns the first 20 invoices.
   *                        A combination of page=2 and page_size=20 returns the next 20 invoices.
   * @param {number} [pageSize=20] - The maximum number of invoices to return in the response.
   * @param {boolean} [totalRequired=true] - Indicates whether the to show total_pages and total_items in the response.
   * @param {string[]} [fields=[]] - List of additional fields to return, if available.
   * @returns {Promise<InvoiceList>}
   *
   * @see [Docs](https://developer.paypal.com/docs/api/invoicing/v2/#invoices_list)
   */
  async listInvoices(
    page = 1,
    pageSize = 20,
    totalRequired = true,
    fields = []
  ) {
    const axios = await this.axios()

    const result = await axios.get(`${this.baseUri}/v2/invoicing/invoices`, {
      params: {
        page,
        page_size: pageSize,
        total_required: totalRequired,
        fields:
          fields.length > 0
            ? fields.reduce(
              (sum, cur, i) => `${sum}${i < fields.length ? "," : ""}${cur}`
            )
            : undefined,
      },
    })

    return new InvoiceList(result.data)
  }

  /**
   * Deletes a draft or scheduled invoice, by ID. Deletes invoices in the
   * draft or scheduled state only. For invoices that have already been
   * sent, you can cancel the invoice. After you delete a draft or scheduled
   * invoice, you can no longer use it or show its details.
   * However, you can reuse its invoice number.
   *
   *
   * @param {string} invoiceID - The ID of the invoice to delete.
   * @returns {Promise<number>} Status code 204 if successful.
   * @see [Docs](https://developer.paypal.com/docs/api/invoicing/v2/#invoices_delete)
   */
  async deleteInvoice(invoiceID) {
    const axios = await this.axios()

    const result = await axios.delete(
      `${this.baseUri}/v2/invoicing/invoices/${invoiceID}`
    )

    return result.status
  }

  /**
   * Fully updates an invoice. This call does not support partial updates.
   *
   * @param {Invoice} invoice - The updated invoice object\
   * @param {boolean} send_to_recipient - Indicates whether to send the invoice update notification to the recipient.
   * @param {boolean} send_to_invoicer - Indicates whether to send the invoice update notification to the merchant.

   * @returns {Promise<Link>} A Link which gives the API endpoint to GET the created invoice. 
   *
   * @see [Docs](https://developer.paypal.com/docs/api/invoicing/v2/#invoices_update)
   */
  async updateInvoice(
    invoice,
    send_to_recipient = true,
    send_to_invoicer = true
  ) {
    const axios = await this.axios()

    const result = await axios.put(
      `${this.baseUri}/v2/invoicing/invoices/${invoice.id}`,
      invoice,
      { params: { send_to_invoicer, send_to_recipient } }
    )

    return result.data
  }

  /**
   * Sends or schedules an invoice, by ID, to be sent to a customer.
   *
   * The action depends on the invoice issue date:
   * - If the invoice issue date is current or in the past, sends the invoice immediately.
   * - If the invoice issue date is in the future, schedules the invoice to be sent on that date.
   *
   * To suppress the merchant's email notification, set the send_to_invoicer body parameter to false.
   * To send the invoice through a share link and not through PayPal, set the send_to_recipient parameter
   * to false in the notification object. The send_to_recipient parameter does not apply to a future
   * issue date because the invoice is scheduled to be sent through PayPal on that date.
   *
   * @param {string} invoiceID - The ID of the invoice to send.
   * @param {string} [subject] - The subject of the email that is sent as a notification to the recipient.
   * @param {string} [note] - A note to the payer.
   * @param {boolean} [send_to_invoicer] - Indicates whether to send a copy of the email to the merchant.
   * @param {boolean} [send_to_recipient=true] - Indicates whether to send a copy of the email to the recipient.
   * @param {{email_address}[]} [additional_recipients] - Note: Valid values are email addresses in the additional_recipients value associated with the invoice.
   *
   * @see [Docs](https://developer.paypal.com/docs/api/invoicing/v2/#invoices_send)
   */
  async sendInvoice(
    invoiceID,
    subject,
    note,
    send_to_invoicer,
    send_to_recipient,
    additional_recipients
  ) {
    const axios = await this.axios()

    const result = await axios.post(
      `${this.baseUri}/v2/invoicing/invoices/${invoiceID}/send`,
      {
        subject,
        note,
        send_to_invoicer,
        send_to_recipient,
        additional_recipients,
      }
    )

    return result.status
  }

  /**
   * Cancels a sent invoice, by ID, and, optionally, sends a notification
   * about the cancellation to the payer, merchant, and CC: emails.
   *
   * @param {string} invoiceID - The ID of the invoice to send.
   * @param {string} [subject="Invoice Cancellation"] - The subject of the email that is sent as a notification to the recipient.
   * @param {string} [note="Invoice cancelled"] - A note to the payer.
   * @param {boolean} [send_to_invoicer=true] - Indicates whether to send a copy of the email to the merchant.
   * @param {boolean} [send_to_recipient=true] - Indicates whether to send a copy of the email to the recipient.
   * @param {{email_address}[]} [additional_recipients] - Note: Valid values are email addresses in the additional_recipients value associated with the invoice.
   * @return {Promise<number>} request status code
   *
   * @see [Docs](https://developer.paypal.com/docs/api/invoicing/v2/#invoices_send)
   */
  async cancelInvoice(
    invoiceID,
    subject = "Invoice Cancellation",
    note = "Invoice cancelled",
    send_to_invoicer = true,
    send_to_recipient = true,
    additional_recipients = []
  ) {
    const axios = await this.axios()

    const d = {
      subject,
      note,
      send_to_invoicer,
      send_to_recipient,
      additional_recipients,
    }

    for (const key in d) {
      if (!d[key]) delete d[key]
    }

    const result = await axios.post(
      `${this.baseUri}/v2/invoicing/invoices/${invoiceID}/cancel`,
      d
    )

    return result.status
  }

  /**
   * Records a payment for the invoice. If no payment is due, the invoice is marked as PAID.
   * Otherwise, the invoice is marked as PARTIALLY PAID.
   * 
   * @param {string} invoiceID - The ID of the invoice to mark as paid
   * @param {"BANK_TRANSFER"|"CASH"|"CHECK"|"CREDIT_CARD"|"DEBIT_CARD"|"PAYPAL"|"WIRE_TRANSFER"|"OTHER"
   * } method - The payment mode or method through which the invoicer can accept the payments.
   * @param {Object} [c]
   * @param {"PAYPAL"|"EXTERNAL"} [c.type="EXTERNAL"] - The PayPal refund type. Indicates whether 
   *                                            the refund was paid through PayPal or externally in the
   *                                            invoicing flow. The record refund method supports the
   *                                            EXTERNAL refund type. The PAYPAL refund type is supported
   *                                            for backward compatibility
   * @param {string} [c.paymentID] - The ID of a PayPal transaction. Required if `type == "PAYPAL"`
   * @param {string} [c.paymentDate] - The date for when the invoice was refunded
   * @param {Amount} [c.amount] - The amount to record as refunded. If you omit the amount, the total
   *                              invoice paid amount is recorded as refunded
   * @param {string} [c.note] - A note associated with an external cash or check payment
   * @param {ShippingInfo} [c.shippingInfo] - The recipient's shipping information. Includes the user's contact information, which includes name and address
   * 
   * @returns {Promise<string>} - The ID of the refund of an invoice payment
   * 
   * @see [Docs](https://developer.paypal.com/docs/api/invoicing/v2/#invoices_refunds)
   */
  async recordPayment(invoiceID, method, {
    type = "EXTERNAL",
    paymentID,
    paymentDate,
    amount,
    note,
    shippingInfo
  } = {}) {
    const axios = await this.axios()

    const result = await axios.post(
      `${this.baseUri}/v2/invoicing/invoices/${invoiceID}/payments`,
      {
        method,
        type,
        payment_id: paymentID,
        payment_date: paymentDate,
        amount,
        shipping_info: shippingInfo,
        note
      }
    )

    return result.data.refund_id
  }

  /**
   * Records a refund for the invoice. If all payments are refunded, the invoice is marked as REFUNDED. 
   * Otherwise, the invoice is marked as PARTIALLY REFUNDED.
   * 
   * *NOTE*: The Invoice MUST have a status of "PAID", "MARKED_AS_PAID", or "PARTIALLY_PAID" to record a refund
   * 
   * @param {string} invoiceID - The ID of the invoice to mark as refunded
   * @param {"BANK_TRANSFER"|"CASH"|"CHECK"|"CREDIT_CARD"|"DEBIT_CARD"|"PAYPAL"|"WIRE_TRANSFER"|"OTHER"
   * } method - The payment mode or method through which the invoicer can accept the payments.
   * @param {Object} [c]
   * @param {"PAYPAL"|"EXTERNAL"} [c.type="EXTERNAL"] - The PayPal refund type. Indicates whether 
   *                                            the refund was paid through PayPal or externally in the
   *                                            invoicing flow. The record refund method supports the
   *                                            EXTERNAL refund type. The PAYPAL refund type is supported
   *                                            for backward compatibility
   * @param {string} [c.refundID] - The ID of a PayPal transaction. Required if `type == "PAYPAL"`
   * @param {string} [c.refundDate] - The date for when the invoice was refunded
   * @param {Amount} [c.amount] - The amount to record as refunded. If you omit the amount, the total
   *                              invoice paid amount is recorded as refunded
   * 
   * @returns {Promise<string>} - The ID of the refund of an invoice payment
   * 
   * @see [Docs](https://developer.paypal.com/docs/api/invoicing/v2/#invoices_refunds)
   */
  async recordRefund(invoiceID, method, {
    type = "EXTERNAL",
    refundID,
    refundDate,
    amount
  } = {}) {
    const axios = await this.axios()

    const result = await axios.post(
      `${this.baseUri}/v2/invoicing/invoices/${invoiceID}/refunds`,
      { method, type, refund_id: refundID, refund_date: refundDate, amount }
    )

    return result.data.refund_id
  }

  /**
   * Deletes an external payment, by invoice ID and transaction ID
   * 
   * @param {string} invoiceID - The ID of the invoice from which to delete an external payment transaction
   * @param {string} transactionID - The ID of the external payment transaction to delete
   * 
   * @see [Docs](https://developer.paypal.com/docs/api/invoicing/v2/#invoices_payments-delete)
   */
  async deleteExternalPayment(invoiceID, transactionID) {
    const axios = await this.axios()

    await axios.delete(`${this.baseUri}/v2/invoicing/invoices/${invoiceID}/payments/${transactionID}`)
  }

  /**
   * Deletes an external refund, by invoice ID and transaction ID.
   * 
   * @param {string} invoiceID - The ID of the invoice from which to delete an external payment transaction
   * @param {string} transactionID - The ID of the external payment transaction to delete
   * 
   * @see [Docs](https://developer.paypal.com/docs/api/invoicing/v2/#invoices_refunds-delete)
   */
  async deleteExternalRefund(invoiceID, transactionID) {
    const axios = await this.axios()

    await axios.delete(`${this.baseUri}/v2/invoicing/invoices/${invoiceID}/refunds/${transactionID}`)
  }

  /**
   * Generates a QR code for an invoice, by ID. The QR code is a PNG image in
   * Base64-encoded format that corresponds to the invoice ID. You can generate
   * a QR code for an invoice and add it to a paper or PDF invoice. When customers
   * use their mobile devices to scan the QR code, they are redirected to the
   * PayPal mobile payment flow where they can view the invoice and pay online with
   * PayPal or a credit card. Before you get a QR code, you must create an invoice
   * and send an invoice to move the invoice from a draft to payable state. Do not
   * include an email address if you do not want the invoice emailed.
   * 
   * @param {string} invoiceID - ID of the Invoice to generate a QR code for
   * @param {number} [width=150] - Width of the QR code
   * @param {number} [height=150] - Height of the QR code
   * @param {"PAY"|"DETAILS"} [action="PAY"] - The type of URL for which to generate a QR code
   * 
   * @return {Promise<string>} - QR code as a PNG image
   */
  async generateQRCode(invoiceID, width = 150, height = width, action = "PAY") {
    const axios = await this.axios()

    const result = await axios.post(
      `${this.baseUri}/v2/invoicing/invoices/${invoiceID}/generate-qr-code`,
      { width, height, action },
    )

    return result.data
  }

  /**
   * Sends a reminder to the payer about an invoice, by ID.
   * The Invoice must have been sent, i.e., Invoice#status == "SENT"
   * 
   * @param {string} invoiceID - ID of the Invoice
   * @param {Object} [c]
   * @param {string} [c.subject] - The subject of the email that is sent as a notification to the recipient
   * @param {string} [c.note] - A note to the payer
   * @param {boolean} [c.sendToInvoicer] - Indicates whether to send a copy of the email to the merchant
   * @param {boolean} [c.sendToRecipient] - Indicates whether to send a copy of the email to the recipient
   * @param {Array<string>} [c.additionalRecipients] - An array of one or more CC: emails to which notifications are sent.
   *                                                  If you omit this parameter, a notification is sent to all CC: email
   *                                                  addresses that are part of the invoice.
   * 
   * @returns {Promise<void>}
   */
  async sendInvoiceReminder(invoiceID, { subject, note, sendToInvoicer, sendToRecipient, additionalRecipients } = {}) {
    const axios = await this.axios()

    await axios.post(
      `${this.baseUri}/v2/invoicing/invoices/${invoiceID}/remind`,
      {
        subject,
        note,
        send_to_invoicer: sendToInvoicer,
        send_to_recipient: sendToRecipient,
        additional_recipients: additionalRecipients
      }
    )
  }

  // Templates
  //----------

  /**
   * Creates an invoice template. You can use details from this template to create an invoice.
   * You can create up to 50 templates.
   * 
   * @param {object} o
   * @param {string} o.id - The ID of the template
   * @param {string} o.name - Template name. 1-500 characters
   * @param {boolean} o.setAsDefault - Indicates whether this template is the default template. A invoicer can have one default template
   * @param {Invoice} o.templateInfo - The template details. Includes invoicer business information, invoice recipients, items, and configuration.
   * TODO typing
   * @param {{template_item_settings: object[], template_subtotal_settings: object[]}} o.settings - The template settings. Describes which fields to show or hide when you create an invoice
   * @param {string} o.unitOfMeasure
   * @param {boolean} o.standardTemplate
   * @param {Link[]} o.links
   * TODO return typing
   * @returns {Promise<any>} - 
   * 
   * @see [Docs](https://developer.paypal.com/docs/api/invoicing/v2/#templates_create)
   */
  async createTemplate({
    id,
    name,
    setAsDefault,
    templateInfo,
    settings,
    unitOfMeasure,
    standardTemplate,
    links
  }) {
    const axios = await this.axios()

    const result = await axios.post(
      `${this.baseUri}/v2/invoicing/templates`,
      {
        id,
        name,
        default_template: setAsDefault,
        template_info: templateInfo,
        settings,
        unit_of_measure: unitOfMeasure,
        standard_template: standardTemplate,
        links
      }
    )

    return result.data
  }
}
