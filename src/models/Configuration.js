import PartialPayment from "./PartialPayment";

export default class Configuration {
  /**
   *
   * @param {Object} C
   * @param {PartialPayment} C.partial_payment
   * @param {boolean} C.allow_tip
   * @param {boolean} C.tax_calculated_after_discount
   * @param {boolean} C.tax_inclusive
   * @param {string} C.template_id
   */
  constructor({
    partial_payment,
    allow_tip,
    tax_calculated_after_discount,
    tax_inclusive,
    template_id,
  }) {
    /**
     * The partial payment details. Includes the minimum amount that the invoicer wants the payer to pay.
     */
    this.partial_payment = partial_payment;
    /**
     * Indicates whether the invoice enables the customer to enter a tip amount during payment.
     *
     * If true, the invoice shows a tip amount field so that the customer can enter a tip amount.
     *
     * If false, the invoice does not show a tip amount field.
     *
     * Note: This feature is not available for users in Hong Kong, Taiwan, India, or Japan.
     */
    this.allow_tip = allow_tip;
    /**
     * Indicates whether the tax is calculated before or after a discount.
     *
     * If false, the tax is calculated before a discount.
     *
     * If true, the tax is calculated after a discount.
     */
    this.tax_calculated_after_discount = tax_calculated_after_discount;
    /** Indicates whether the unit price includes tax. */
    this.tax_inclusive = tax_inclusive;
    /**
     * The template ID. The template determines the layout of the invoice.
     * Includes which fields to show and hide.
     */
    this.template_id = template_id;
  }
}
