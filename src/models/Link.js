
/**
 * @since 1.0.0
 * @author JonoAugustine
 *
 * @see [Docs](https://developer.paypal.com/docs/api/invoicing/v2/#definition-link_description)
 */
export default class Link {
  /**
 *
 * @param {Object} L
 * @param {string} L.href
 * @param {string} L.rel
 * @param {"GET"|"POST"|"PUT"|"DELETE"|"HEAD"|"CONNECT"|"OPTIONS"|"PATCH"} L.method
 */
  constructor({ href, rel, method }) {
    /**
     * The complete target URL. To make the related call, combine the method with
     * this URI Template-formatted link. For pre-processing, include the
     * $, (, and ) characters. The href is the key HATEOAS component that links a
     * completed call with a subsequent call.
     */
    this.href = href
    /**
     * The link relation type, which serves as an ID for a link that unambiguously
     * describes the semantics of the link.
     */
    this.rel = rel
    /**
     * The HTTP method required to make the related call.
     * @type {"GET"|"POST"|"PUT"|"DELETE"|"HEAD"|"CONNECT"|"OPTIONS"|"PATCH"}
     */
    this.method = method
  }
}
