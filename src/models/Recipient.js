import BillingInfo from "./BillingInfo";
import ShippingInfo from "./ShippingInfo";

/**
 * Model of invoice recipient.
 *
 * @see [docs](https://developer.paypal.com/docs/api/invoicing/v2/#definition-recipient_info)
 * @since 1.0.0
 * @author JonoAugustine
 */
export default class Recipient {
  constructor({ billing_info, shipping_info }) {
    /**
     * The billing information for the invoice recipient.
     * Includes name, address, email, phone, and language.
     */
    this.billing_info = billing_info;
    /**
     * The recipient's shipping information. Includes the user's
     * contact information, which includes name and address.
     */
    this.shipping_info = shipping_info;
  }
}
