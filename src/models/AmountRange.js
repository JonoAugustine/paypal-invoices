import Amount from "./Amount"

/**
 * @since 1.1.11
 * @author JonoAugustine
 */
export default class AmountRange {
  /**
   * 
   * @param {Amount} lowerBound
   * @param {Amount} upperBound
   */
  constructor(lowerBound, upperBound) {
    this.lower_amount = lowerBound
    this.upper_amount = upperBound
  }
}
