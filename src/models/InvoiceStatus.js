export default Object.freeze({
  /**
   * The invoice is in draft state. It is not yet sent to the payer,
   */
  DRAFT: "DRAFT",
  /**
   * The invoice has been sent to the payer. The payment is awaited from the payer,
   */
  SENT: "SENT",
  /**
   * The invoice is scheduled on a future date. It is not yet sent to the payer,
   */
  SCHEDULED: "SCHEDULED",
  /**
   * The payer has paid for the invoice,
   */
  PAID: "PAID",
  /**
   * The invoice is marked as paid by the invoicer,
   */
  MARKED_AS_PAID: "MARKED_AS_PAID",
  /**
   * The invoice has been cancelled by the invoicer,
   */
  CANCELLED: "CANCELLED",
  /**
   * The invoice has been refunded by the invoicer,
   */
  REFUNDED: "REFUNDED",
  /**
   * The payer has partially paid for the invoice,
   */
  PARTIALLY_PAID: "PARTIALLY_PAID",
  /**
   * The invoice has been partially refunded by the invoicer,
   */
  PARTIALLY_REFUNDED: "PARTIALLY_REFUNDED",
  /**
   * The invoice is marked as refunded by the invoicer,
   */
  MARKED_AS_REFUNDED: "MARKED_AS_REFUNDED",
  /**
   * The invoicer is yet to receive the payment from the payer for the invoice,
   */
  UNPAID: "UNPAID",
  /**
   * The invoicer is yet to receive the payment for the invoice. It is under pending review,
   */
  PAYMENT_PENDING: "PAYMENT_PENDING",
})