/** Metadata of invoice Detail given in API responses. */
export default class DetailMetadata {
  /**
   * @param {Object} Metadata
   * @param {string} Metadata.create_time - The date and time of creation in ISO
   * @param {string} Metadata.recipient_view_url - The URL for the payer's view
   * @param {string} Metadata.invoicer_view_url - The URL for the seller's view
   */
  constructor({ create_time, recipient_view_url, invoicer_view_url }) {
    /** The date and time when the resource was created, in Internet date and time format. */
    this.create_time = create_time;
    /** The URL for the invoice payer view hosted on paypal.com */
    this.recipient_view_url = recipient_view_url;
    /** The URL for the invoice merchant view hosted on paypal.com. */
    this.invoicer_view_url = invoicer_view_url;
  }
}
