/** Model of a person's name */
export default class Name {
  constructor({ given_name, surname }) {
    this.given_name = given_name;
    this.surname = surname;
  }
}
