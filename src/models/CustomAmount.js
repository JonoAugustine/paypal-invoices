import Amount from "./Amount";

export default class CustomAmount {
  constructor({ label, amount }) {
    this.label = label;
    this.amount = amount;
  }
}
