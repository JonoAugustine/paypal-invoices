import DetailPaymentTerm from "./DetailPaymentTerm";



/**
 * The details of the invoice.
 * Includes the invoice number, date, payment terms, and audit metadata.
 *
 * @since 1.0.0
 * @author JonoAugustine
 */
export default class Detail {
  /**
   *
   * @param {Object} D
   *
   * @param {string} D.invoice_number
   * @param {string} D.reference
   * @param {string} D.invoice_date
   * @param {string} D.currency_code
   * @param {string} D.note
   * @param {string} D.term
   * @param {string} D.memo
   * @param {DetailPaymentTerm} D.payment_term
   * @param {DetailMetadata} D.metadata
   *
   */
  constructor({
    invoice_number,
    reference,
    invoice_date,
    currency_code,
    note,
    term,
    memo,
    payment_term,
    metadata,
  }) {
    /** Unique Invoice Number (Which is alpha-numeric) */
    this.invoice_number = invoice_number;
    /** The reference data. Includes a post office (PO) number. */
    this.reference = reference;
    /**
     * The invoice date as specificed by the sender, in Internet date and time format.
     * Only UTC is supported in response. For example, yyyy-MM-ddTz.
     */
    this.invoice_date =
      typeof invoice_date === "object"
        ? invoice_date.toISOString()
        : invoice_date;
    this.currency_code = currency_code;
    /** A note to the invoice recipient. Also appears on the invoice notification email. */
    this.note = note;
    /**
     * The general terms of the invoice. Can include return or cancellation
     * policy and other terms and conditions.
     */
    this.term = term;
    /** A private bookkeeping memo for the user. */
    this.memo = memo;
    /** The payment due date for the invoice. Value is either but not both term_type or due_date. */
    this.payment_term = payment_term;
    /** The audit metadata. Captures all invoicing actions on create, send, update, and cancel. */
    this.metadata = metadata;
  }
}
