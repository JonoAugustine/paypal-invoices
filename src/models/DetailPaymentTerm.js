/** The payment due date for the invoice. Value is either but not both term_type or due_date. */
export default class DetailPaymentTerm {
  /**
   * @param {Object} Term
   * @param {"NET_10" | "NET_15" | "NET_30" | "NET_45" | "NET_60" | "NET_90" | "NO_DUE_DATE" | "DUE_ON_DATE_SPECIFIED" | "DUE_ON_RECEIPT"} Term.term_type - The payment term. Payment can be due upon receipt, a specified date, or in a set number of days.
   * @param {string} Term.due_date
   */
  constructor({ term_type, due_date }) {
    /**
     * The payment term. Payment can be due upon receipt, a specified date, or in a set number of days.
     *
     * The possible values are:
     *
     *  - DUE_ON_RECEIPT. The payment for the invoice is due upon receipt of the invoice.
     *  - DUE_ON_DATE_SPECIFIED. The payment for the invoice is due on the date specified in the invoice.
     *  - NET_10. The payment for the invoice is due in 10 days.
     *  - NET_15. The payment for the invoice is due in 15 days.
     *  - NET_30. The payment for the invoice is due in 30 days.
     *  - NET_45. The payment for the invoice is due in 45 days.
     *  - NET_60. The payment for the invoice is due in 60 days.
     *  - NET_90. The payment for the invoice is due in 90 days.
     *  - NO_DUE_DATE. The invoice has no payment due date.
     *
     * @type {"NET_10" | "NET_15" | "NET_30" | "NET_45" | "NET_60" | "NET_90" | "NO_DUE_DATE" |
     *         "DUE_ON_DATE_SPECIFIED" | "DUE_ON_RECEIPT"}
     */
    this.term_type = term_type;
    /**
     * The date when the invoice payment is due, in Internet date and time
     * format. For example, yyyy-MM-ddTz.
     */
    this.due_date = due_date;
  }
}
