import Amount from "./Amount"
import Configuration from "./Configuration"
import Detail from "./Detail"
import Invoicer from "./Invoicer"
import Item from "./Item"
import Link from "./Link"
import Recipient from "./Recipient"

/**
 * Invoice Model
 *
 * @see [docs](https://developer.paypal.com/docs/api/invoicing/v2/)
 * @since 1.0.0
 * @author JonoAugutine
 */
export default class Invoice {
  /**
   * @param {Object} I
   * @param {string} I.id
   * @param {"PAYMENT_PENDING"|"UNPAID"|"MARKED_AS_REFUNDED"|"PARTIALLY_REFUNDED"|"PARTIALLY_PAID"|"REFUNDED"|
   *          "CANCELLED"|"MARKED_AS_PAID"|"PAID"|"SCHEDULED"|"SENT"|"DRAFT"} I.status
   * @param {Detail} I.detail
   * @param {Invoicer} I.invoicer
   * @param {Recipient[]} I.primary_recipients
   * @param {Item[]} I.items
   * @param {Configuration} I.configuration
   * @param {Amount} I.amount
   * @param {Amount} I.due_amount
   * @param {Link[]} I.links
   */
  constructor({
    id,
    status,
    detail,
    invoicer,
    primary_recipients,
    items,
    configuration,
    amount,
    due_amount,
    links = [],
  }) {
    this.id = id
    this.status = status
    this.detail = detail
    this.invoicer = invoicer
    this.primary_recipients = primary_recipients
    this.items = items
    this.configuration = configuration
    this.amount = amount
    this.due_amount = due_amount
    this.links = links
  }
}
