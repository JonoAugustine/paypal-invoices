import Amount from "./Amount";
import CustomAmount from "./CustomAmount";
import Discount from "./Discount";
import ShippingCost from "./ShippingCost";

export default class AmountBreakdown {
  /**
   *
   * @param {Object} AB
   * @param {Amount} AB.item_total
   * @param {Discount} AB.discount
   * @param {Amount} AB.tax_total
   * @param {ShippingCost} AB.shipping
   * @param {CustomAmount} AB.custom
   */
  constructor({ item_total, discount, tax_total, shipping, custom }) {
    this.item_total = item_total;
    this.discount = discount;
    this.tax_total = tax_total;
    this.shipping = shipping;
    this.custom = custom;
  }
}
