/**
 * Model of OAuth information returned by PayPal token gen.
 *
 * @since 1.0.0
 * @author JonoAugustine
 */
export default class OAuthToken {
  constructor(tokenData) {
    this.scope = tokenData.scope;
    /**
     * OAuth token string.
     *
     * @type {string}
     */
    this.accessToken = tokenData.access_token;
    this.type = tokenData.token_type;
    this.appId = tokenData.app_id;
    /**
     * The number of seconds after which the token will expire.
     *
     * @type {number}
     */
    this.expiresIn = tokenData.expires_in;
    /**
     * The number of minutes after which the token will expire.
     *
     * @type {number}
     */
    this.expiresInMin = this.expiresIn / 60;
    this.createdAt = Date.now();
  }

  /** @returns {number} The number of minutes until the token expires. */
  minutesLeft() {
    /** milliseconds between creation and now */
    const lifeTimeMill = this.createdAt - Date.now();

    /** minutes between creation and now */
    const lifeTimeMin = Math.round(
      ((lifeTimeMill % 86400000) % 3600000) / 60000
    );

    // Return the remain min
    return this.expiresInMin - lifeTimeMin;
  }
}
