import Amount from "./Amount";
import Discount from "./Discount";
import Tax from "./Tax";

export default class Item {
  /**
   * @param {Object} I
   * @param {string} I.name
   * @param {string} I.description
   * @param {string} I.quantity
   * @param {Amount} I.unit_amount
   * @param {Tax} I.tax
   * @param {Discount} I.discount
   * @param {"QUANTITY" | "HOURS" | "AMOUNT"}  I.unit_of_measure
   */
  constructor({
    name,
    description,
    quantity,
    unit_amount,
    tax,
    discount,
    unit_of_measure,
  }) {
    this.name = name;
    this.quantity = quantity;
    this.description = description;
    this.unit_amount = unit_amount;
    this.tax = tax;
    this.discount = discount;
    this.unit_of_measure = unit_of_measure;
  }
}
