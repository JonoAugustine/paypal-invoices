/**
 * @author JonoAugustine
 * 
 * @since 1.1.11
 */
export default class DateTimeRange {
  /**
   * 
   * @param {string} start - Start of date range
   * @param {string} end - End of date range
   */
  constructor(start, end) {
    this.start = start
    this.end = end
  }
}