import Address from "./Address";
import Name from "./Name";
import Phone from "./Phone";

/**
 * The invoicer information.
 * Includes the business name, email, address, phone, fax, tax ID, additional notes, and logo URL.
 *
 * @since 1.0.0
 * @author JonoAugustine
 */
export default class Invoicer {
  /**
   *
   * @param {Object} I
   * @param {Name} I.name
   * @param {Address} I.address
   * @param {string} I.email_address
   * @param {Phone[]} I.phones
   * @param {string} I.website - The invoicer's website.
   * @param {string} I.tax_id - The invoicer's tax id.
   * @param {string} I.logo_url
   * @param {string} I.additional_notes
   *
   */
  constructor({
    name,
    address,
    email_address,
    phones,
    website,
    tax_id,
    logo_url,
    additional_notes,
  }) {
    this.name = name;
    this.address = address;
    this.email_address = email_address;
    this.phones = phones;
    this.website = website;
    this.tax_id = tax_id;
    this.logo_url = logo_url;
    this.additional_notes = additional_notes;
  }
}
