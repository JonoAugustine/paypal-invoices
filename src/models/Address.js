import AddressDetails from "./AddressDetails";

/**
 * @since 1.0.0
 * @author JonoAugustine
 */
export default class Address {
  /**
   *
   * @param {Object} A
   * @param {string} A.address_line_1
   * @param {string} A.address_line_2
   * @param {string} A.address_line_3
   * @param {string} A.admin_area_1
   * @param {string} A.admin_area_2
   * @param {string} A.admin_area_3
   * @param {string} A.admin_area_4
   * @param {string} A.postal_code
   * @param {string} A.country_code
   * @param {string} A.address_details
   */
  constructor({
    address_line_1,
    address_line_2,
    address_line_3,
    admin_area_1,
    admin_area_2,
    admin_area_3,
    admin_area_4,
    postal_code,
    country_code,
    address_details,
  }) {
    /**
     * The first line of the address. For example, number or street.
     * For example, 173 Drury Lane. Required for data entry and compliance
     * and risk checks. Must contain the full address.
     */
    this.address_line_1 = address_line_1;
    /**
     * The second line of the address. For example, suite or apartment number.
     */
    this.address_line_2 = address_line_2;
    /**
     * The third line of the address, if needed. For example, a street
     * complement for Brazil, direction text, such as next to Walmart,
     * or a landmark in an Indian address.
     */
    this.address_line_3 = address_line_3;
    /**
     * The highest level sub-division in a country, which is usually a province, state, or ISO-3166-2 subdivision. Format for postal delivery.
     * For example, CA and not California.
     *
     * Value, by country, is:
     * - UK. A county.
     * - US. A state.
     * - Canada. A province.
     * - Japan. A prefecture.
     * - Switzerland. A kanton.
     */
    this.admin_area_1 = admin_area_1;

    /** A city, town, or village. Smaller than admin_area_level_1. */
    this.admin_area_2 = admin_area_2;
    /**
     * A sub-locality, suburb, neighborhood, or district. Smaller than admin_area_level_2.
     *
     * Value is:
     * - Brazil. Suburb, bairro, or neighborhood.
     * - India. Sub-locality or district. Street name information is
     * not always available but a sub-locality or district can be a very small area.
     */
    this.admin_area_3 = admin_area_3;
    /**
     * The neighborhood, ward, or district. Smaller than admin_area_level_3
     * or sub_locality.
     *
     * Value is:
     *  - The postal sorting code for Guernsey and
     * many French territories, such as French Guiana.
     *  - The fine-grained
     * administrative levels in China.
     */
    this.admin_area_4 = admin_area_4;
    /**
     * The postal code, which is the zip code or equivalent. Typically
     * required for countries with a postal code or an equivalent.
     * @see [postal code](https://en.wikipedia.org/wiki/Postal_code).
     */
    this.postal_code = postal_code;
    /**
     * The two-character ISO 3166-1 code that identifies the country or region.
     *
     * NOTE:
     * - The country code for Great Britain is GB and not UK as used in the
     * top-level domain names for that country. Use the C2 country code for
     * China worldwide for comparable uncontrolled price (CUP) method,
     * bank card, and cross-border transactions.
     *
     * @see [docs](https://developer.paypal.com/docs/api/reference/country-codes/)
     */
    this.country_code = country_code;
    /**
     * The non-portable additional address details that are sometimes needed
     * for compliance, risk, or other scenarios where fine-grain address
     * information might be needed. Not portable with common third party and
     * open source. Redundant with core fields. For example,
     * address_portable.address_line_1 is usually a combination of
     * address_details.street_number, street_name, and street_type.
     */
    this.address_details = address_details;
  }
}
