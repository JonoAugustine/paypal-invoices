/**
 * Model of Phone data.
 *
 * @since 1.0.0
 * @author JonoAugustine
 */
export default class Phone {
  /**
   *
   * @param {Object} P
   * @param {string} P.country_code
   * @param {string} P.national_number
   * @param {string} P.extension_number
   * @param {"FAX" | "HOME" | "MOBILE" | "OTHER" | "PAGER"} P.phone_type
   */
  constructor({ country_code, national_number, extension_number, phone_type }) {
    /**
     * The country calling code (CC), in its canonical international
     * E.164 numbering plan format. The combined length of the CC and
     * the national number must not be greater than 15 digits.
     * The national number consists of a national destination code (NDC)
     * and subscriber number (SN).
     */
    this.country_code = country_code;
    /**
     * The national number, in its canonical international E.164 numbering
     * plan format. The combined length of the country calling code (CC)
     * and the national number must not be greater than 15 digits.
     * The national number consists of a national destination code (NDC)
     * and subscriber number (SN).
     */
    this.national_number = national_number;
    this.extension_number = extension_number;
    this.phone_type = phone_type;
  }
}
