import Amount from "./Amount";

export default class PartialPayment {
  /**
   *
   * @param {Object} PP
   * @param {Amount} PP.minimum_amount_due
   * @param {boolean} PP.allow_partial_payment
   */
  constructor({ allow_partial_payment, minimum_amount_due }) {
    this.allow_partial_payment = allow_partial_payment;
    this.minimum_amount_due = minimum_amount_due;
  }
}
