import Amount from "./Amount";
import Tax from "./Tax";

export default class ShippingCost {
  constructor({ amount, tax }) {
    this.amount = amount;
    this.tax = tax;
  }
}
