import Link from "./Link";
import Invoice from "./Invoice";

export default class InvoiceList {
  /**
   *
   * @param {Object} IL
   * @param {number} IL.total_pages
   * @param {number} IL.total_items
   * @param {Invoice[]} IL.items
   * @param {Link[]} IL.links
   */
  constructor({ total_pages, total_items, items, links }) {
    this.total_pages = total_pages;
    this.total_items = total_items;
    this.items = items || [];
    this.links = links || [];
  }
}
