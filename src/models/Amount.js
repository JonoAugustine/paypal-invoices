import AmountBreakdown from "./AmountBreakdown";

/**
 *
 * @see [docs](https://developer.paypal.com/docs/api/invoicing/v2/#definition-money)
 * @since 1.0.0
 * @author JonoAugustine
 */
export default class Amount {
  /**
   * @param {Object} A
   * @param {string} A.currency_code
   * @param {string} A.value
   * @param {AmountBreakdown} A.breakdown
   */
  constructor({ currency_code, value, breakdown }) {
    /**
     * The three-character ISO-4217 currency code that identifies the currency.
     */
    this.currency_code = currency_code;
    /**
     * The value, which might be:
     * - An integer for currencies like JPY that are not typically fractional.
     * - A decimal fraction for currencies like TND that are subdivided into thousandths.
     */
    this.value = value;
    /**
     * The breakdown of the amount. Breakdown provides details such as total item amount,
     * total tax amount, custom amount, shipping and discounts, if any.
     */
    this.breakdown = breakdown ? new AmountBreakdown(breakdown) : undefined;
  }
}
