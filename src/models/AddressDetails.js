/**
 * @since 1.0.0
 * @author JonoAugustine
 */
export default class AddressDetails {
  constructor({
    street_number,
    street_name,
    street_type,
    delivery_service,
    building_name,
    sub_building,
  }) {
    this.street_number = street_number;
    this.street_name = street_name;
    /** The street type. For example, avenue, boulevard, road, or expressway. */
    this.street_type = street_type;
    /** The delivery service. Post office box, bag number, or post office name. */
    this.delivery_service = delivery_service;
    /**
     * A named locations that represents the premise. Usually a building name
     * or number or collection of buildings with a common name or number.
     * For example, Craven House.
     */
    this.building_name = building_name;
    /**
     * The first-order entity below a named building or location that represents
     * the sub-premise. Usually a single building within a collection of
     * buildings with a common name. Can be a flat, story, floor, room, or
     * apartment.
     */
    this.sub_building = sub_building;
  }
}
