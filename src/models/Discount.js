import Amount from "./Amount";

export default class Discount {
  constructor({ percent, amount }) {
    this.percent = percent;
    this.amount = amount;
  }
}
