import Address from "./Address";
import Name from "./Name";

export default class ShippingInfo {
  /**
   * 
   * @param {Object} i
   * @param {Name} i.name
   * @param {Address} i.address
   */
  constructor({ name, address }) {
    this.name = name;
    this.address = address;
  }
}
