import Amount from "./Amount";

export default class Tax {
  /**
   *
   * @param {Object} T
   * @param {string} T.name
   * @param {string} T.percent
   * @param {Amount} T.amount
   */
  constructor({ name, percent, amount }) {
    this.name = name;
    /** The tax rate. Value is from 0 to 100. Supports up to five decimal places. */
    this.percent = percent;
    this.amount = amount;
  }
}
