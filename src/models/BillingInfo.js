import Name from "./Name";
import Phone from "./Phone";

export default class BillingInfo {
  /**
   *
   * @param {Object} BI
   * @param {string} BI.business_name
   * @param {Name} BI.name
   * @param {Address} BI.address
   * @param {string} BI.email_address
   * @param {Phone[]} BI.phones
   * @param {string} BI.additional_info
   * @param {string} BI.language
   */
  constructor({
    business_name,
    name,
    address,
    email_address,
    phones,
    additional_info,
    language,
  }) {
    this.business_name = business_name;
    this.name = name;
    this.address = address;
    /**
     * The invoice recipient email address. If you omit this value,
     * the invoice is payable and a notification email is not sent.
     */
    this.email_address = email_address;
    /**
     * The invoice recipient's phone numbers. Extension number is not supported.
     */
    this.phones = phones;
    this.additional_info = additional_info;
    /**
     * The recipient's shipping information. Includes the user's
     * contact information, which includes name and address.
     */
    this.language = language;
  }
}
