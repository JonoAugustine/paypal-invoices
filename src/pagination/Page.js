import { Link } from "../models"

/**
 * Models the return of a paginated route.
 * 
 * @since 1.0.0
 * @author JonoAugustine
 */
export default class Page {
  /**
   * 
   * @param {Object} p
   * @param {number} p.total_items
   * @param {number} p.total_pages
   * @param {Link} p.links
   * @param {*} items
   */
  constructor({ total_items, total_pages, links, items }) {
    this.total_items = total_items
    this.total_pages = total_pages
    this.links = links
    this.items = items
  }
}