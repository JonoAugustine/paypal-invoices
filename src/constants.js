/** Root URI for sandbox API */
export const URI_ROOT_SANDBOX = "https://api.sandbox.paypal.com";

/** Root URI for live API */
export const URI_ROOT_LIVE = "https://api.paypal.com";

/** Path for OAuth API */
export const OAUTH_PATH = "/v1/oauth2/token";

/** Time left on token expiration before it should refresh */
export const TOKEN_TIME_LIMIT = 5;
