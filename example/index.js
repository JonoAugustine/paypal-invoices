require("dotenv").config();

const { Invoices, Invoicer } = require("../out/bundle");
const { sampleInvoice } = require("./sample");

const api = new Invoices(
  process.env.CLIENT_ID,
  process.env.CLIENT_SECRET
  //true
);

const main = async () => {
  await api.initialize().catch((e) => console.log(e, "failed"));

  api.defaultInvoicer(
    new Invoicer({
      name: {
        given_name: "Jon",
        surname: "Snow",
      },
      email_address: process.env.INVOICER_EMAIL,
    })
  );

  const invoiceNumber = await api.generateInvoiceNumber();
  // Create a new Invoice draft
  const link = await api.createDraftInvoice(
    sampleInvoice(
      invoiceNumber,
      process.env.INVOICER_EMAIL,
      "swordmaster9@gmail.com"
    )
  );

  // Get the created invoice
  const invoiceDraft = await api.getInvoiceByLink(link);

  await api.sendInvoice(invoiceDraft.id);

  const deleted = await api.cancelInvoice(invoiceDraft.id);

  console.log(deleted);
};

main()
  .then(() => {})
  .catch((e) => {
    console.log(e, "Failed");
  });
