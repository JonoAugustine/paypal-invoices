exports.sampleInvoice = (
  invoiceNumber,
  fromEmail = "gib-money@example.com",
  toEmail = "bill-me@example.com"
) => ({
  detail: {
    invoice_number: invoiceNumber,
    currency_code: "USD",
    payment_term: { term_type: "NET_10" },
    invoice_date: new Date().toISOString().split("T")[0],
  },
  invoicer: {
    name: {
      given_name: "David",
      surname: "Larusso",
    },
    email_address: fromEmail,
    website: "www.test.com",
  },
  primary_recipients: [
    {
      billing_info: {
        name: {
          given_name: "Stephanie",
          surname: "Meyers",
        },
        email_address: toEmail,
      },
    },
  ],
  items: [
    {
      name: "Yoga Mat",
      description: "Elastic mat to practice yoga.",
      quantity: "1",
      unit_amount: {
        currency_code: "USD",
        value: "50.00",
      },
      unit_of_measure: "QUANTITY",
    },
  ],
});
