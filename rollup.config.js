import resolve from "@rollup/plugin-node-resolve";

export default {
  input: "src/index.js",
  output: {
    file: "./out/bundle.js",
    format: "cjs",
  },
  plugins: [resolve({ dedupe: ["models", "pagination"] })],
  external: ["axios"],
};
