# paypal-invoices

An api wrapper for paypal 2.0 invoices.

## Installing

```nolang
npm i paypal-invoices
```

## Getting Started

```javascript
const { Invoices } = require('paypal-invoices')

const invoice = {...}

const main = async () => {
  // Create a new API instance
  const api = new Invoices(CLIENT_ID, CLIENT_SECRET)
  // Or a sandbox api
  // const api = new Invoices(CLIENT_ID, CLIENT_SECRET, true)

  // Initialize the API
  try {
    await api.initialize();
  } catch (e) {
    console.log("Could not initialize");
    return;
  }

  // Get the next Invoice number
  const invoiceNum = await api.generateInvoiceNumber();

  // Create a new Invoice draft
  const link = await api.createDraftInvoice(/* Invoice Object*/);

  // Get the created invoice
  const invoiceDraft = await api.getInvoiceByLink(link);

  // Send the new Invoice to the recipient
  await api.sendInvoice(invoiceDraft.id);
};

main();
```

---

## Logging

To enabled request logging, add the following environment variable.

```env
PP_INVOICER:LOG=true
```

---

[PayPal official docs](https://developer.paypal.com/docs/api/invoicing/v2/#invoices_create)

## Roadmap

- Pagination
- Templates
- Data Validation

## Dependencies

- [axios](https://www.npmjs.com/package/axios)
